# SuperRentals

[SuperRenatals](http://nappy-donkey.surge.sh/rentals) is pure Ember.js I created basing on [official guide](https://guides.emberjs.com/v2.9.0/).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `$ git clone git@bitbucket.org:marek_parafianowicz/super-rentals.git`
* `$ cd super-rentals`
* `$ npm install`
* `$ bower install`

## Running / Development

* `$ ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `$ ember help generate` for more details

### Running Tests

* `$ ember test`
* `$ ember test --server`

### Building

* `$ ember build` (development)
* `$ ember build --environment production` (production)

### Deploying

I used surge to deploy this application - [SuperRentals](http://nappy-donkey.surge.sh/rentals).
To deploy it in similar way, perform following steps:

* You will need surge cli tool installed: `$ npm install -g surge`

* Deploy the folder produced by `ember build`. Surge also need copy of index.html with the filename 200.html to suppor client-side routing.

```bash
$ ember build --environment=development
$ cd dist
$ cp index.html 200.html
$ surge
```

* Surge will generate you an URL. To deploy to the same URL after making changes in application, you will need to:

```bash
$ rm -rf dist
$ ember build --environment=development
$ cd dist
$ cp index.html 200.html
$ surge funny-name.surge.sh
```


## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

